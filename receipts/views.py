from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from receipts.forms import CreateReceipt, CreateExpenseCategory, CreateAccount
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required
def receipt_list(request):
    receipt = Receipt.objects.filter(purchaser=request.user)
    print(receipt)
    context = {
        "receipt_list": receipt,
    }
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = CreateReceipt(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            form.save()
            return redirect("home")

    else:
        form = CreateReceipt()

    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    print(categories)
    context = {
        "category_list": categories,
    }
    return render(request, "categories/list.html", context)


@login_required
def account_list(request):
    account = Account.objects.filter(owner=request.user)
    print(account)
    context = {
        "account_list": account,
    }
    return render(request, "accounts/list.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = CreateExpenseCategory(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            form.save()
            return redirect("category_list")

    else:
        form = CreateExpenseCategory()

    context = {
        "form": form,
    }
    return render(request, "categories/create.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = CreateAccount(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            form.save()
            return redirect("account_list")

    else:
        form = CreateAccount()

    context = {
        "form": form,
    }
    return render(request, "accounts/create.html", context)
